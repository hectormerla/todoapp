'use strict';

/**
 * @ngdoc service
 * @name todoappApp.taskFactory
 * @description
 * # taskFactory
 * Factory in the todoappApp.
 */
angular.module('todoappApp')
  .factory('taskFactory', function ($resource) {

    return $resource("http://localhost/todoapi/public/tasks/:id", { id: '@_id' }, {
      update: {
        method: 'PUT' // this method issues a PUT request
      }
    });
  });