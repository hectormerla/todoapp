'use strict';

/**
 * @ngdoc function
 * @name todoappApp.controller:TaskCtrl
 * @description
 * # TaskCtrl
 * Controller of the todoappApp
 */
angular.module('todoappApp')
  .controller('TaskCtrl', function ($scope, taskFactory) {
    
   //  var tasks = taskFactory.query(function() {
   //  	console.log(tasks);
		// $scope.tasks = tasks;   
  	// }); 

  	$scope.task = {
  		'title': '',
  		'description': ''
  	};
  	$scope.active = true;

  	$scope.tasks = [{"id":1,"title":"do todo app","description":"do a todo app for a test with laravel and angular","created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00"},{"id":2,"title":"do todo app","description":"do a todo app for a test with laravel and angular","created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00"},{"id":3,"title":"some task","description":"some description","created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00"},{"id":4,"title":"other task","description":"other description","created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00"}];


  	$scope.addTask = function(){
  		 $scope.tasks.push($scope.task);
  		// taskFactory.save($scope.task);
  	};

  	$scope.editTask = function(){
  		$scope.active = false;
  	};

  	$scope.updateTask = function($index){
  		$scope.active = true;
  		// get task
  		// $update method
  	};

  	$scope.deleteTask = function($index){
  		$scope.tasks.splice($index,1);
  		// get task
  		// $delete method
  	};


  });
